# Título do projeto

Um parágrafo da descrição do projeto, o que ele faz.

[Wiki do projeto](https://google.com/)

## Pré requisitos

Que coisas são necessárias para instalar o software e como instalá-las.

* [Maven](https://maven.apache.org/) - Dependency Management versão X.X

## Começando

Instruções para adquirir uma cópia do projeto rodando no ambiente local para propósitos de desenvolvimento e teste.
Exemplo, colocar parâmetros de banco tal, VM options.

```
Dar exemplos, parâmetros...
```

## Extras

* [Link do gerador de versão](http://100.1.1.196:8087)
* Como rodar Sonar local:
* Rodar como Televendas:

## Projetos dependentes

[Nome do projeto](http:gitlab....)